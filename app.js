const express = require("express");
const app = express();

app.use(express.json());

app.listen(3000, () => {
  console.log("Server listening the port 3000");
});


function  fibonacci(n)
    {
       
        let f = new Array(n+2); 
        let i;
        f[0] = 0;
        f[1] = 1;
        for (i = 2; i <= n; i++)
        {
            f[i] = f[i-1] + f[i-2];
        }
        return f[n];
    }

    function BalancedStringPartition(str, n)
    {
 

        if (n == 0)
            return 0;
        let r = 0, l = 0;
 
        let ans = 0;
        for(let i = 0; i < n; i++)
        {
           if (str[i] == 'R')
           {
               r++;
           }
           else if (str[i] == 'L')
           {
               l++;
           }
           if (r == l)
           {
               ans++;
           }
        }
        return ans;
    }

app.get("/fibonacci/:id", async (req,res)=>{
    res.json({data:await fibonacci(req.params.id),message:'Data retrieved'});

});

app.get("/balanced-string-partition/:string", async (req,res)=>{
    const string = req.params.string;
    const stringLength = string.length;
        res.json({data:await BalancedStringPartition(req.params.string,stringLength),message:'Data retrieved'});
    
    });
module.exports = app;
